#ifndef SDLController_h
#define SDLController_h
///
/// @file SDLController.h
///
//------------------------------------------------------------------------------
// Copyright 2012 Rohit Dubey (rodydubey@gmail.com)
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//------------------------------------------------------------------------------

#include <SDL/SDL.h>
#include <boost/shared_ptr.hpp>
#include <iostream>
#include "almotionexample.h"

namespace imi
{
    /// Handles the input for controlling the player
    /// Uses SDL joystick if available, also accepts keyboard arrows.
    class SDLController
    {
    public:
        SDLController( void );
        
        ~SDLController();
    
        void handlePlayerInteraction( SDL_Event& event, ALMotionExample *myMotion );
        
    private:
        SDL_Joystick* mController;
        SDL_GLContext ctx;
        float mCurrentSpeed;
        float mCurrentTurn;
        float mLastFacing;
        float mMaxPlayerSpeed;
        bool mIsIntialized;
    };
    typedef boost::shared_ptr< SDLController > SDLControllerPtr;
}

#endif
