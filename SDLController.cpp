///
/// @file SDLController.cpp
///
//------------------------------------------------------------------------------
// Copyright 2012 Rohit Dubey (rodydubey@gmail.com)
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//------------------------------------------------------------------------------

#include "SDLController.h"
#include <cmath>

using namespace imi;

SDLController
::SDLController()
: mController(NULL)
{
    SDL_Init( SDL_INIT_EVERYTHING );

    if( SDL_NumJoysticks() > 0 )
    {
        SDL_JoystickEventState(SDL_ENABLE);
        mController = SDL_JoystickOpen( 0 );
    }
    else
    {
        std::cerr << "\nNo Joystick/gamepad.\nHit any key to exit the application\n";
        getchar(); 
        exit(0);
    }
   
}

SDLController
::~SDLController()
{
    if( mController ) SDL_JoystickClose( mController );
    SDL_Quit();
}

void
SDLController
::handlePlayerInteraction( SDL_Event& event, ALMotionExample *myMotion )
{
    const int maxJoystickValue = 32768; // Defined by SDL
    float amt = (float)event.jaxis.value / (float)maxJoystickValue;
    bool flag = false;
    switch( event.type )
    {
        flag = true;
        case SDL_JOYAXISMOTION:
        if(SDL_JoystickGetAxis(mController, 0) < -3200)/* left up */
        {
            myMotion->nonBlockWalking(1.0,0.0,0,1);
        }
        else if(SDL_JoystickGetAxis(mController, 0) > 3200)/* left down */
        {
            myMotion->nonBlockWalking(-1.0,0.0,0,1); // back
        }

        if(SDL_JoystickGetAxis(mController, 1) < -3200) /* left left */
        {
            myMotion->nonBlockWalking(amt,0.0,-amt*3,1);
        }
        else if(SDL_JoystickGetAxis(mController, 1) > 3200)/* left right */
        { 
            myMotion->nonBlockWalking(amt,0.0,-amt*3,1);
        }
        if(SDL_JoystickGetAxis(mController, 3) < -3200) /* Right Joystick left */
        {            
            myMotion->moveHeadYaw(-amt/2);
        }
        else if(SDL_JoystickGetAxis(mController, 3) > 3200)/* Right Joystick right */
        {
            myMotion->moveHeadYaw(-amt/2);
        }
        if(SDL_JoystickGetAxis(mController, 2) < -3200) /* Right Joystick left */
        {            
            myMotion->moveHeadPitch(-amt/2);
        }
        else if(SDL_JoystickGetAxis(mController, 2) > 3200)/* Right Joystick right */
        {
            myMotion->moveHeadPitch(-amt/2);
        }       
        break;
        case SDL_JOYBUTTONDOWN:  /* Handle Joystick Button Presses */
            if( SDL_JoystickGetButton(mController, 0) ) /* A */
        {
            myMotion->stiffnessOnPoseInit(); // init
		}

		if( SDL_JoystickGetButton(mController, 1) ) /* B */
        {
            myMotion->poseZero();
		}

		if( SDL_JoystickGetButton(mController, 2) )/* X */
        {
            myMotion->nonBlockWalking(0.0,0.0,0,1); // stop walking
		}

		if( SDL_JoystickGetButton(mController, 3) ) /* Y */
        {

		}

		if( SDL_JoystickGetButton(mController, 4) ) { /* L */
		}

		if( SDL_JoystickGetButton(mController, 5) ) { /* R */
		}

		if( SDL_JoystickGetButton(mController, 6) ) { /* Home */
		}

		if( SDL_JoystickGetButton(mController, 7) ) { /* Hold */
					
		}

		if( SDL_JoystickGetButton(mController, 8) ) { /* Help 1 */
		}

		if( SDL_JoystickGetButton(mController, 9) ) { /* Help 2 */
		}

		if( SDL_JoystickGetButton(mController, 10) ) { /* Stick Press */
		}
        break;
        case SDL_KEYDOWN:
        switch( event.key.keysym.sym )
        {
        case SDLK_LEFT:
                
            break;
        case SDLK_RIGHT:
                
            break;
        case SDLK_UP:
               
            break;
        case SDLK_DOWN:
               
            break;
        }
        break;
        case SDL_KEYUP:
        switch( event.key.keysym.sym )
        {
        case SDLK_UP:
               
            break;
        case SDLK_LEFT:
                
            break;
        case SDLK_RIGHT:
               
            break;
        }
        break;

        default:
        {
            //myMotion->nonBlockWalking(0.0,0.0,0,1); // stop walking
            //std::cout << "huiuuiguiiiggi \n";
        }
    }
}



