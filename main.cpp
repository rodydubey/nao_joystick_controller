///
/// @file main.cpp
///
//------------------------------------------------------------------------------
// Copyright 2012 Rohit Dubey (rodydubey@gmail.com)
//
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//------------------------------------------------------------------------------
#include <iostream>
#include <signal.h>
#include <boost/shared_ptr.hpp>
#include <alcommon/albroker.h>
#include <alcommon/almodule.h>
#include <alcommon/albrokermanager.h>
#include <alcommon/altoolsmain.h>
#include <qi/os.hpp>
#include "almotionexample.h"
#include <SDL/SDL.h>
#include "SDLController.h"

#include <iostream>
#include <string>

using namespace AL;
#ifdef MOTIONEXAMPLE_IS_REMOTE
# define ALCALL
#else
# ifdef _WIN32
#  define ALCALL __declspec(dllexport)
# else
#  define ALCALL
# endif
#endif
using namespace imi;
extern "C"
{

ALCALL int _createModule(boost::shared_ptr<AL::ALBroker> pBroker)
{
  // init broker with the main broker instance
  // from the parent executable
  AL::ALBrokerManager::setInstance(pBroker->fBrokerManager.lock());
  AL::ALBrokerManager::getInstance()->addBroker(pBroker);
  AL::ALModule::createModule<ALMotionExample>( pBroker, "ALMotionExample");
  return 0;
}

ALCALL int _closeModule()
{
  return 0;
}

} // extern "C"


#ifdef MOTIONEXAMPLE_IS_REMOTE

int main(int argc, char *argv[])
{
    if(argc < 2)
    {
    std::cerr << "Usage: custommain NAO_IP" << std::endl;
    return 2;
    }

    SDLControllerPtr interactionController( new SDLController() );

    // We will try to connect our broker to an running NAOqi:
    const std::string parentBrokerIP = std::string(argv[1]);
    int parentBrokerPort = 9559;

    setlocale(LC_NUMERIC, "C");

    // A broker needs a name, an IP and a port:
    const std::string brokerName = "mybroker";
    // FIXME: would be a good idea to look for a free port first
    int brokerPort = 54000;
    const std::string brokerIp   = "0.0.0.0";  // listen to anything

    boost::shared_ptr<AL::ALBroker> broker;
    try
    {
    broker = AL::ALBroker::createBroker(
        brokerName,
        brokerIp,
        brokerPort,
        parentBrokerIP,
        parentBrokerPort,
        0    // you can pass various options for the broker creation,
                // but default is fine
        );
    }
    catch(const AL::ALError& /* e */)
    {
    std::cerr << "Faild to connect broker to: "
                << parentBrokerIP
                << ":"
                << parentBrokerPort
                << std::endl;
    AL::ALBrokerManager::getInstance()->killAllBroker();
    AL::ALBrokerManager::kill();
    return 1;
    }

    // Deal with ALBrokerManager singleton:
    AL::ALBrokerManager::setInstance(broker->fBrokerManager.lock());
    AL::ALBrokerManager::getInstance()->addBroker(broker);

    try
    {
        AL::ALModule::createModule<ALMotionExample>( broker, "ALMotionExample");

        ALMotionExample myMotion(broker, "ALMotionExample");

        uint32_t currMillisecs = SDL_GetTicks();
        uint32_t prevMillisecs = currMillisecs;
        float dtimeSeconds;
        bool isRunning=true;
     
        while(isRunning)
        {
            currMillisecs = SDL_GetTicks();
            dtimeSeconds = 0.001f * float( currMillisecs - prevMillisecs ); // assume GetTicks gives ms
            prevMillisecs = currMillisecs;
            if(dtimeSeconds > 0.1f)
            {
                dtimeSeconds = 0.016f;
                // TODO: This is practically an error-- frames are taking more than 0.1 seconds
            }
            SDL_Event event;
        
            while(SDL_PollEvent(&event))
            { 
                interactionController->handlePlayerInteraction(event,&myMotion);
            }
        }
    }
    catch (const AL::ALError& e)
    {
        std::cerr << "Caught exception: " << e.what() << std::endl;
        return 1;
    }
  return 0;
}
#endif
